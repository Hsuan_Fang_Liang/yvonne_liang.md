This diary file is written by Edwin Ke in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* Tried to come up with a few mediators, but turns out I was confused with the definition of a mediator.
* Our group didn't get to do the presentation. Next time, we will be faster to grab the chance.
* The layouts of a PowerPoint presentation can really affect its outcome.
* This is my first time using BitBucket. Might take a while to get used to this tool.

# 2021-09-30 #

* Think it would be a bit difficult for every person to present 3 times, since many students have enrolled in this class. 
* Guess I'll just have to be fast and grasp the chance to present.
* There is always going to be negativity on social media, so it's important to learn to not be greatly affected.
* Since I am a highly sensitive person, I definitely need to be aware of the negative effects of social media.
* Gave a presentation and received lots of feedback from the professor. Glad to have learned more about how to make better slides.
* The world is getting better gradually, but even with the problem of famine being solved, I believe we still have a long way to go.
* Learned that one good way to deal with fake news is by comparing alternative sources.
* Statistics can be inaccurate amd misleading if the surveys conducted to produce them are ambiguous.
