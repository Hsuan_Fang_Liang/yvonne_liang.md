This diary file is written by Val Cheah E14067042 in the course Professional skills for engineering the third industrial revolution.

# 2020-09-17 #
* I'd like to point out that while win-win conflicts are certainly admirable, there are cases where someone might be blatantly wrong, and having them lose the argument in a win-lose conflict might result in them realising the problem, and be a better outcome long term.
* Knowing what disease we are predisposed to is great and all but what would happen if someone doesnt take the information well and fall into a "I am going to die might as well live the rest of my life recklessly" situation. Seems like it would be a problem especially if national welfare allows everyone to do a genome screening.
* We've always said that the current year is the "worst year ever". I wonder if people in the future will agree that 2020 is, in fact, the worst year ever. Statistically there *is* going to be a "worst year ever", right?

# 2020-9-24 #

* I haven't thought about how integrating population into car accident deaths data can be expressed by a log graph.
* Professor mentioned that he thinks journalist are not as good as movie makers are in invoking joy, and knowing how to sell joy would be a business opportunity. It made me think of the success of John Kraskinski's Some Good News. Now that that show has ended, I hope that there will be successors.
* The girl in the video is dangerously trusting of the government. Eventhough it might be true that private companies investigate too small a sample, government statistics might not be impartial. They are not immune to having ulterior motives and manipulating narratives.
* Journalist's professional conduct is underated.

# 2020-10-08 #
* The dinosaur feeding picture is cute.
* I didn't know about Bretton Woods.
* The video claims that broad money is a better idea of money.
* The video also claims that QE is not free money and that QE leading to money multiplication is a myth.
* The second video had amazing narrative animation but I didn't agree with the logic of it.
* I thought that redistributing wealth sounded like the bullying the minority, who happens to be rich.

# 2020-10-15 #
* It's refreshing to see Switzerland doing well.
* I think that wealth inequality will inevitably exist as it's just result of the bell curve phenomena.
* The video claims that nationalism is helpful towards a stable society, unlike John Lennon's point of view.
* The video claims that nationalism is an identity that overrides all other identity.
* It thinks that the dictatorships have the ability to control data, which we should be careful about.
* However, I like to think that innovation of the democratic population will prevail.
* I agree that democracy is based on feelings.
* The other video claims that humans live in a dual reality, the objective and imaginative.
* I think that the physical reality is only a really tiny part of our lives.

# 2020-10-22 #
* I was unwell and was absent this week.
* Exercise video claims that the effects are immediate and can take effect after one single session.

# 2020-10-28 #
* I think it wasn't really practical trying to construct a powerpoint in class because there are so many technical variables, like lack of proper equipment and file type.
* I think it's hard to use a list to tell if someone is suffering from depression.
* If it's innapropriate to interview someone currently suffering from depression, I thought that if would also make some of the students in class uncomfortable if they happen to also be suffering from some kind of mental illness at the moment.
* I've seen the TED talk of the officer on the bridge many times already.

# 2020-11-12 #
* I felt sick and overslept this week.

# 2020-11-19 #
* I don't really like the the way the first talk.
* I don't think free information is socialist. It's more of an argument about intellectual property.
* I've seen the second video.
* People in the past also gets handed dukes and other positions positions and be called special just because they're born into it.
* Some parts of the talk sounds condensending when referring to the younger generation, as a lesser "they". It's not going to get the point across.
* I like the third speaker. She has a an argument, and relied less on humor and emotion to present a point. Somehow emotional talks make me uncomfortable.

# 2020-11-26 #
* I like this point of view but unfortunately people don't talk about this more.
* The fiat currency happens to be my father's area of study, so growing up I've heard a lot about it from him.

# 2020-12-03 #
* I felt like the videos rely too much on shock value and jargons.

# 2020-12-10 #
* I like the presentation this week.
* The debate is refreshing and fun.
* I like the interaction and chances to make a point.

# 2020-12-17 #
* I think that one of the reasons there wasn't much debate between the three classes was the choice of topics. I think there wasn't as much diversion in the three parties regarding the topics chose.
* Daily Dairy
  
1. 201217 Thu
	1. Successful and productive  
	2. I succesfully woke up early. I also did my homework in time.
	3. I hope to wake up early again tomorrow.
  
2. 201218 Fri
	1. Unsuccessful and unproductive  
	2. I woke up late. I also couldn't stay awake in class. I don't remember what I learnt in class. 
	3. I wish to wake up early again.    
  
3. 201219 Sat
	1. Successful and unproductive  
	2. I woke up at a reasonable time and went to see a friend at a zero waste market. I then went to the Chimei Christmas Event with some friends and took some good pictures. Then I met with a friend who came over from Taichung and drank till 4am. Fulfilled my social quota for the week.
	3. Wake up in the morning to shop with friend.    
  
4. 201220 Sun
	1. Unsuccessful and unproductive  
	2. Woke up in time to call a friend in US. But fell asleep again and woke up at 6pm. Didn't accomplish anything today.
	3. I have to do at least some homework tomorrow.    
  
5. 201221 Mon
	1. Unsuccessful and unproductive  
	2. Had a brunch with friend and finally shopped for the wallet she wanted. Put the physiotherapy appointment on rain check and didn't attend club session. Did not do homework.
	3. Have to finish homework tomorrow.    
  
6. 201222 Tue
	1. Unsuccessful and unproductive  
	2. Woke up late and procrastinated. Went to physiotherapy and did not get my homework done in time. Slept very late trying to do at least some of the homework. However, still procrastinated by editing the photos taken over the weekend instead of doing homework.
	3. Attend class tomorrow and sleep earlier.    
  
7. 201223 Wed
	1. Unsuccessful and productive  
	2. Overslept and did not attend class. However did help out with club stuff and attended group work session with teammates.
	3. Fix my sleep schedule.
	
* I am aware of my huge problem concerning scheduling and sleep. I do try.

# 2020-12-24 #
* I forgot to write my diary this week.

# 2021-1-07 #
Theme 1 - “A world of facts, challenges, and ideas”  

* Know the current world demographics  
Have a pop quiz/Who-want's-to-be-a-Millionaire-like game in class about world demographic facts.

* Ability to use demographics to explain engineering needs  
Think up and discuss examples where one innovation doesn't work in another environment.

* Understand planetary boundaries and the current state  
Show arguments provided by people who deny that the world is in danger and have the class disprove the arguments or come up with counter arguments.

* Understand how the current economic system fails to distribute resources  
Conduct a simulation in class, with each student allocated with different amount of "money" at the start, to simulate different economic class. Provide opportunities to spend the money, like education, investment, luxury, and tally the amount everyone has at the end of the experiment. Analyise the data and see if any conclusion can be found.

* Be familiar with future visions and their implications, such as artificial intelligence  
Discuss how technologies are depicted in different science fiction and scifi movies. Write a list of technologies or ideas in science fiction and classify them into those that already exist, plausible given time, and pure fiction.


Theme 2 - “Personal health, happiness, and society”  

* Understand how data and engineering enable a healthier life  
Discuss what data collected through social media or other means can be used to improve human health. It doens't have to be a complete solution, just a concept.

* Know that social relationships gives a 50% increased likelihood of survival  
Since most people now make many friends online, discuss internet relationships, the difference between relationships made in real life, and the safety of it all.

* Be familiar with depression and mental health issues  
Have the class to come up with misconception about mental issues, and disprove them.

* Know the optimal algorithm for finding a partner for life  
Watch and discuss how practical are Jubilee dating experiments.

Theme 3 - “Professional skills to success”  

* Develop a custom of questioning claims to avoid “fake news”  
Create a poster for steps to identify fake news and share it.

* Be able to do basic analysis and interpretation of time series data  
Conduct some interviews, research and polls to find a trend in NCKU during the last few years, be it the change of restaurants in Yule Street, the number of students enrolling and the percentage of male to female freshman or other data, and visualise it in a graph.

* Experience of collaborative and problem-based learning  
Discuss and figure out what can be improved in the department, what we can do or who do we have to contact and push for the change.

* Understand that professional success depends on social skills  
Talk about body language, postures, tones and other charismatic skills.

* Know that the culture of the workplace affect performance  
Have students share their work experience, and poll on the kind of workplace they prefer, whether it's the high paying but often boring TSMC, modest paying smaller company with empathatic employers, whether there is carrer potential, major relevance and other factors.
